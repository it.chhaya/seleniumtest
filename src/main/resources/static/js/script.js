$(function() {
    console.log('Ready')
    getResult()
})

function getResult() {

    $.ajax({
        url: "http://localhost:8080/result",
        method: "GET",
        success: function(res) {
            $('tbody').html(`<tr><td>${res[0].homeTeam}</td></tr>`)
        },
        error: function(err) {
            console.log(err)
        }
    })

}

// var div = `<div class="table-wrapper ng-scope" id="table1" ng-repeat=" (k,v) in items | orderBy:'v.schedule'|filter : filterByLeague | groupBy : 'league' as filteredItems " style="width: 48%;">
// <table>
//   <thead>
//     <tr>
//       <th colspan="8" style="font-size: 10px;font-weight: 800" class="ng-binding">${res.title}</th>
//     </tr>
//   </thead>
//   <tbody>
//     <!-- ngRepeat:  team in v  --><tr ng-repeat=" team in v " style="font-weight: 800;" class="ng-scope">
//       <td width="5%" align="center"></td>
//       <td width="32.5%" style="padding-left: 17px;vertical-align: top !important;" class="ng-binding">
//         <span ng-class="team.isAwayTorOy==false?(team.neutral==false?'left-black-box':''):''" class="left-black-box"></span>Southampton</td>
//       <td align="center" width="5%" class="ng-binding">106</td>
//       <td width="7.5%" align="center" class="ng-binding">0</td>
//       <td align="center" width="5%" class="ng-binding">87</td>
//       <td width="32.5%" style="padding-left: 5px" class="ng-binding">Leeds United<span ng-class="team.isAwayTorOy==true?(team.neutral==false?'right-black-box':''):''"></span></td>
//       <td width="5%" align="center"></td>
//       <td width="7.5%" align="center" class="ng-binding">00:00</td>
//     </tr><!-- end ngRepeat:  team in v  --><tr ng-repeat=" team in v " style="font-weight: 800;" class="ng-scope">
//       <td width="5%" align="center"></td>
//       <td width="32.5%" style="padding-left: 17px;vertical-align: top !important;" class="ng-binding">
//         <span ng-class="team.isAwayTorOy==false?(team.neutral==false?'left-black-box':''):''" class="left-black-box"></span>Manchester United</td>
//       <td align="center" width="5%" class="ng-binding">105</td>
//       <td width="7.5%" align="center" class="ng-binding">1½</td>
//       <td align="center" width="5%" class="ng-binding">88</td>
//       <td width="32.5%" style="padding-left: 5px" class="ng-binding">Fulham<span ng-class="team.isAwayTorOy==true?(team.neutral==false?'right-black-box':''):''"></span></td>
//       <td width="5%" align="center"></td>
//       <td width="7.5%" align="center" class="ng-binding">00:00</td>
//     </tr><!-- end ngRepeat:  team in v  --><tr ng-repeat=" team in v " style="font-weight: 800;" class="ng-scope">
//       <td width="5%" align="center"></td>
//       <td width="32.5%" style="padding-left: 17px;vertical-align: top !important;" class="ng-binding">
//         <span ng-class="team.isAwayTorOy==false?(team.neutral==false?'left-black-box':''):''"></span>Manchester City</td>
//       <td align="center" width="5%" class="ng-binding">111</td>
//       <td width="7.5%" align="center" class="ng-binding">1+1½</td>
//       <td align="center" width="5%" class="ng-binding">83</td>
//       <td width="32.5%" style="padding-left: 5px" class="ng-binding">Brighton &amp; Hove Albion<span ng-class="team.isAwayTorOy==true?(team.neutral==false?'right-black-box':''):''" class="right-black-box"></span></td>
//       <td width="5%" align="center"></td>
//       <td width="7.5%" align="center" class="ng-binding">01:00</td>
//     </tr><!-- end ngRepeat:  team in v  --><tr ng-repeat=" team in v " style="font-weight: 800;" class="ng-scope">
//       <td width="5%" align="center"></td>
//       <td width="32.5%" style="padding-left: 17px;vertical-align: top !important;" class="ng-binding">
//         <span ng-class="team.isAwayTorOy==false?(team.neutral==false?'left-black-box':''):''" class="left-black-box"></span>Chelsea</td>
//       <td align="center" width="5%" class="ng-binding">93</td>
//       <td width="7.5%" align="center" class="ng-binding">½+1</td>
//       <td align="center" width="5%" class="ng-binding">100</td>
//       <td width="32.5%" style="padding-left: 5px" class="ng-binding">Leicester City<span ng-class="team.isAwayTorOy==true?(team.neutral==false?'right-black-box':''):''"></span></td>
//       <td width="5%" align="center"></td>
//       <td width="7.5%" align="center" class="ng-binding">02:15</td>
//     </tr><!-- end ngRepeat:  team in v  -->
//   </tbody>
// </table>
// </div>`