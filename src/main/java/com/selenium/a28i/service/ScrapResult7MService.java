package com.selenium.a28i.service;

import java.util.ArrayList;
import java.util.List;

import com.selenium.a28i.model.Result7M;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScrapResult7MService {
    private final ChromeDriver driver;
    Result7M temp = null;

    @Autowired
    public ScrapResult7MService(ChromeDriver driver) {
        this.driver = driver;
    }

    public List<Result7M> scrapResult7M(String url) {
        List<Result7M> results = new ArrayList<>();
        driver.get(url);

        // wait page to load table 50sec
        WebElement tableContents= new WebDriverWait(driver, 50).until(ExpectedConditions.visibilityOfElementLocated(By.id("result_tb")));

        List<WebElement> trs = tableContents.findElements(By.tagName("tr"));
        System.out.println(trs.size());
        for(int i=0;i<trs.size();i++){
            temp = new Result7M();
            List<WebElement> tds =trs.get(i).findElements(By.tagName("td"));
            System.out.println(tds.size());
            for(int j=0;j<tds.size();j++){
                if(j==2){
                    WebElement homeTeam = tds.get(j).findElement(By.tagName("a"));
                    temp.setHomeTeam(homeTeam.getText());
                    System.out.println(homeTeam.getText());
                }else if(j==3){
                    WebElement result = tds.get(j).findElement(By.tagName("b"));
                    temp.setResult(result.getText());
                    System.out.println(result.getText());
                }else if(j==4){
                    WebElement awayTeam = tds.get(j).findElement(By.tagName("a"));
                    temp.setAWayTeam(awayTeam.getText());
                    System.out.println(awayTeam.getText());
                }
            }
            results.add(temp);
        }
        
        return results;
    }

}
