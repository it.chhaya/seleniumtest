package com.selenium.a28i.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.selenium.a28i.model.Result;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByTagName;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScrapResultService {
    private final ChromeDriver driver;
    Result temp = null;

    @Autowired
    public ScrapResultService(ChromeDriver driver) {
        this.driver = driver;
    }

    public List<Result> scrapResult(final String url) {

        List<Result> results = new ArrayList<>();

        driver.get(url);

        // open a28i homepage ,fill credential and login
        final WebElement form = driver.findElementById("login-form");
        final WebElement button = driver.findElementByClassName("jss15");
        final WebElement usernameInput = form.findElement(By.id("username"));
        final WebElement passwordInput = form.findElement(By.id("password"));

        usernameInput.sendKeys("hgarun66002");
        passwordInput.sendKeys("123abc");

        button.click();
        // open odds page and click Football(Early)
        new WebDriverWait(driver, 50).until(ExpectedConditions.urlToBe("https://web.a28i.com/terms"));
        driver.get("https://web.a28i.com/results");

        // wait 50sec for result href to load
        // new WebDriverWait(driver, 50).until(
        // ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='navbar-upper']//li[1]//a")));
        // final WebElement resultBtn =
        // driver.findElement(By.xpath("//ul[@class='navbar-upper']//li[1]//a"));
        // resultBtn.click();

        // wait 50secs for btn yesterday load
        WebElement formResult = new WebDriverWait(driver, 100)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("form#form")));
        WebElement yesterdayBtn = formResult.findElement(By.xpath("//form[@id='form']//button[2]"));

        yesterdayBtn.click();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement tableContent = new WebDriverWait(driver, 100)
                .until(ExpectedConditions.visibilityOfElementLocated(By.tagName("table")));

        List<WebElement> trs = tableContent.findElements(By.xpath("//tr[count(td)>2])"));
        System.out.println(trs.size());

        for (int i = 0; i < trs.size(); i++) {
            System.out.println("Row = " + i);
            temp = new Result();
            List<WebElement> tds = trs.get(i).findElements(By.tagName("td"));
            System.out.println(tds.size());
            for (int j = 0; j < tds.size(); j++) {
                if (j == 0) {
                    System.out.println("j = 0, skipped");
                    continue;
                }
                if (j == 1) {
                    try {
                        System.out.println("j = 1, found result");
                        WebElement team = tds.get(j).findElement(By.tagName("p"));
                        System.out.println(tds.get(j).findElement(By.tagName("p")).getText());
                        String[] homeAndAwayTeam = team.getText().split(" vs ", 2);
                        String homeTeam = homeAndAwayTeam[0];
                        String awayTeam = homeAndAwayTeam[1];
                        temp.setHomeTeam(homeTeam);
                        temp.setAwayTeam(awayTeam);
                        System.out.println("home team = " + homeTeam + " / away team = " + awayTeam);
                        continue;
                    } catch (NoSuchElementException e) {
                        System.out.println(e.getMessage());
                        continue;
                    }
                }
                if (j == 2) {
                    System.out.println("j = 2, skipped");
                    continue;
                }
                if (j == 3) {
                    System.out.println("j = 3, get result of full match");
                    WebElement result = tds.get(j).findElement(By.tagName("p")).findElement(By.tagName("span"));
                    temp.setResult(result.getText());
                }
            }
            results.add(temp);
        }

        // for(int i=0;i<trs.size();i++){
        // temp= new Result();
        // List<WebElement> tds = trs.get(i).findElements(By.tagName("td"));
        // for(int j=0;j<tds.size();j++){
        // if(j==1){
        // WebElement team = tds.get(j).findElement(By.tagName("p"));
        // String[] res = (team.getText()).split(" vs ",2);
        // String homeTeam = res[0];
        // String awayTeam =res[1];
        // temp.setHomeTeam(homeTeam);
        // System.out.println(homeTeam);

        // temp.setAwayTeam(awayTeam);
        // System.out.println(awayTeam);
        // }else if(j==3){

        // try {
        // WebElement result = tds.get(j).findElement(By.tagName("span"));
        // temp.setResult(result.getText());
        // System.out.println(result.getText());
        // } catch (NoSuchElementException e) {
        // temp.setResult("8-8");
        // System.out.println("Not complete!");
        // }

        // }

        // }
        // results.add(temp);

        // }

        return results;
    }
}
