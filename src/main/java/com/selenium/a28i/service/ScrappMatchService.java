package com.selenium.a28i.service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.selenium.a28i.model.Item;
import com.selenium.a28i.model.Match;

import org.apache.xalan.xsltc.compiler.Template;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScrappMatchService {

    private final ChromeDriver driver;

    @Autowired
    public ScrappMatchService(ChromeDriver driver) {
        this.driver = driver;
    }

    public List<Match> scrape(final String url) {

        // define model as list
        List<Match> matches = new ArrayList<>();
        // List<Match> matches = new ArrayList<>();
        Match tempMatch = null;

        driver.get(url);

        // open a28i homepage ,fill credential and login
        final WebElement form = driver.findElementById("login-form");
        final WebElement button = driver.findElementByClassName("jss15");
        final WebElement usernameInput = form.findElement(By.id("username"));
        final WebElement passwordInput = form.findElement(By.id("password"));

        usernameInput.sendKeys("hgarun66001");
        passwordInput.sendKeys("abc123");

        button.click();

        LocalTime changeMarketTime = LocalTime.of(11, 0);
        LocalTime now = LocalTime.now();

        // open odds page and click Football(Early)
        new WebDriverWait(driver, 50).until(ExpectedConditions.urlToBe("https://web.a28i.com/terms"));
        driver.get("https://web.a28i.com/odds");

        if (now.isAfter(changeMarketTime)) {
            // wait 50seconds for link football today located
            new WebDriverWait(driver, 50).until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='firstMenu']//li[1]//a")));
            final WebElement parleyTodayBtn = driver.findElement(By.xpath("//ul[@class='secondMenu']//li[2]//a"));
            parleyTodayBtn.click();

        } else {
            // wait 50seconds for link football early located
            new WebDriverWait(driver, 50).until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='firstMenu']//li[2]//a")));
            final WebElement footballEarlyHref = driver.findElement(By.xpath("//ul[@class='firstMenu']//li[2]//a"));
            // click href football early
            footballEarlyHref.click();

            // click parley from football early href
            new WebDriverWait(driver, 50).until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='secondMenu']//li[2]//a")));
            final WebElement parleyBtn = driver.findElement(By.xpath("//ul[@class='firstMenu']//li[2]//a"));
            parleyBtn.click();
        }
        WebElement secondTableContent = new WebDriverWait(driver, 50)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.SecondTableGroup")));

        // retrieve all tbody that have li > 2 elements as list
        // List<WebElement> tbodies =
        // secondTableContent.findElements(By.cssSelector("tbody"));
        List<WebElement> tbodies = secondTableContent
                .findElements(By.xpath("//div[@class='secondTable']//tbody[count(tr)>2]"));
        // System.out.println("total tbodies " + tbodies.size());
        String tempLeagueTitle = "";
        boolean isHasTitle = true;
        // Loop table body using for
        for (int i = 0; i < tbodies.size(); i++) {
            tempMatch = new Match();

            if (isHasTitle == false)
                tempMatch.setLeague(tempLeagueTitle);
            // retrieve all tr elements as list
            List<WebElement> trs = tbodies.get(i).findElements(By.cssSelector("tr"));
            // System.out.println("total trs :" + trs.size());

            // loop table tr using for

            for (int j = 0; j < trs.size() - 1; j++) {
                int k = 0;

                if (trs.size() == 4) {
                    WebElement leagueTitle = trs.get(0).findElement(By.cssSelector("th[colspan='9'] span"));

                    if (leagueTitle.isDisplayed()) {
                        tempMatch.setLeague(leagueTitle.getText());
                        tempLeagueTitle = leagueTitle.getText();
                    }
                    k = 1;
                    isHasTitle = false;
                }
                if (j == 0 + k) {
                    List<WebElement> tds = trs.get(j).findElements(By.tagName("td"));
                    for (int l = 0; l < tds.size(); l++) {
                        if (l == 0) {
                            List<WebElement> playTimes = tds.get(l).findElements(By.tagName("p"));
                            tempMatch.setSchedule(playTimes.get(0).getText()+" "+playTimes.get(1).getText());

                        } else if (l == 1) {
                            WebElement homeTeam = tds.get(l).findElement(By.tagName("p"));
                            tempMatch.setHomeTeam(homeTeam.getText());
                            if (homeTeam.getText().contains("(n)")) {
                                tempMatch.setNeutral(true);
                            }
                        } else if (l == 2) {
                            List<WebElement> oddAndPercents = tds.get(l).findElements(By.tagName("p"));
                            tempMatch.setOdd(oddAndPercents.get(0).getText());
                            tempMatch.setHomePercent(Float.parseFloat(oddAndPercents.get(1).getText()));
                        }
                    }
                } else if (j == 1 + k) {
                    List<WebElement> td_1s = trs.get(j).findElements(By.tagName("td"));
                    for (int m = 0; m < td_1s.size(); m++) {
                        if (m == 0) {
                            WebElement awayTeam = trs.get(j).findElement(By.tagName("p"));
                            tempMatch.setAwayTeam(awayTeam.getText());
                        } else if (m == 1) {
                            List<WebElement> oddAndPercent_1s = td_1s.get(m).findElements(By.tagName("p"));
                            if (oddAndPercent_1s.get(0).getText().length() > 0) {
                                tempMatch.setOdd(oddAndPercent_1s.get(0).getText());
                                tempMatch.setAwayTorOy(true);
                            }
                            tempMatch.setAwayPercent(Float.parseFloat(oddAndPercent_1s.get(1).getText()));
                        }
                    }

                }

                // System.out.println(tempMatch.getLeague());
                // System.out.println("play at " + tempMatch.getSchedule());
                // System.out.println("home team " + tempMatch.getHomeTeam());
                // System.out.println("home percent " + tempMatch.getHomePercent());
                // System.out.println("away team " + tempMatch.getAwayTeam());
                // System.out.println("away percent " + tempMatch.getAwayPercent());
                // System.out.println("odd " + tempMatch.getOdd());

            }
            matches.add(tempMatch);
        }

        // driver.quit();
        return matches;

    }

    public String convertOdd(String oldOdd) {
        String newOdd = "";
        switch (oldOdd) {
            case "0/0.5":
                newOdd = "0+½";
                break;
            case "0.5":
                newOdd = "½";
                break;
            case "0.5/1":
                newOdd = "½+1";
                break;
            case "1/1.5":
                newOdd = "1+1½";
                break;
            case "1.5":
                newOdd = "1½";
                break;
            case "1.5/2":
                newOdd = "1½+2";
                break;
            case "2/2.5":
                newOdd = "2+2½";
                break;
            case "2.5":
                newOdd = "2½";
                break;
            case "2.5/3":
                newOdd = "2½+3";
                break;
            case "3/3.5":
                newOdd = "3+3½";
                break;
            case "3.5":
                newOdd = "3½";
                break;
            case "3.5/4":
                newOdd = "3½+4";
                break;
            case "4/4.5":
                newOdd = "4+4½";
                break;
            case "4.5":
                newOdd = "4½";
                break;
            case "4.5/5":
                newOdd = "4½+5";
                break;
            case "5/5.5":
                newOdd = "5+5½";
                break;
            case "5.5":
                newOdd = "5½";
                break;
            case "5.5/6":
                newOdd = "5½+6";
                break;
            case "1.0":
                newOdd = "1";
                break;
            case "2.0":
                newOdd = "2";
                break;
            case "3.0":
                newOdd = "3";
                break;
            case "4.0":
                newOdd = "4";
                break;
            case "5.0":
                newOdd = "5";
                break;
            case "6.0":
                newOdd = "6";
                break;
            case "7.0":
                newOdd = "7";
                break;
            case "0.0":
                newOdd = "0";
                break;
        }
        return newOdd;
    }

    public String convertPercent(String oldPercent) {
        String newPercent = "";
        newPercent = String.valueOf(Math.floor(Float.parseFloat(oldPercent) * 100));
        return newPercent;
    }
}
