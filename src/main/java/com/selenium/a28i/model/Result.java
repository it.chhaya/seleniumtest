package com.selenium.a28i.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Result {
    private String awayTeam;
    private String homeTeam;
    private String result;
}
