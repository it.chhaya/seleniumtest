package com.selenium.a28i.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Result7M {
    private String homeTeam;
    private String aWayTeam;
    private String result;
}
