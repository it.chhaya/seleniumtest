package com.selenium.a28i.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Match {
    private String odd;
    private String league;
    private boolean neutral=false;
    private String awayTeam;
    private String homeTeam;
    private String schedule;
    private float awayPercent;
    private float homePercent;
    private boolean isAwayTorOy=false;

}
