package com.selenium.a28i.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Item {
    private String title;
    private Match match;
}
