package com.selenium.a28i;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class A28IApplication {

	public static void main(String[] args) {
		SpringApplication.run(A28IApplication.class, args);
	}

}
