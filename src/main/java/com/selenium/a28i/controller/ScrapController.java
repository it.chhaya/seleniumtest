package com.selenium.a28i.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.selenium.a28i.model.Match;
import com.selenium.a28i.model.Result;
import com.selenium.a28i.model.Result7M;
import com.selenium.a28i.service.ScrapResult7MService;
import com.selenium.a28i.service.ScrapResultService;
import com.selenium.a28i.service.ScrappMatchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScrapController {

    private final String url = "https://web.a28i.com/";
    private final String url1="https://data.7msport.com/result/default_en.shtml?date=2021-05-17";

    private ScrappMatchService scrapMatchService;
    private ScrapResultService scrapResultService;
    private ScrapResult7MService scrapResult7MService;
    

    @Autowired
    public ScrapController(ScrappMatchService scrapMatchService, ScrapResultService scrapResultService,ScrapResult7MService scrapResult7MService) {
        this.scrapMatchService = scrapMatchService;
        this.scrapResultService = scrapResultService;
        this.scrapResult7MService=scrapResult7MService;
    }
 
 
    
    @GetMapping("/match")
    public ResponseEntity<List<Map<String, Object>>> viewMatchPage() {

        Map<String, Object> api = null;
        List<Map<String, Object>> response = new ArrayList<>();

        List<Match> matches = scrapMatchService.scrape(url);

        for (Match match : matches) {
            api = new HashMap<>();
            System.out.println(match);
            api.put("awayPercent", match.getAwayPercent());
            api.put("awayTeam", match.getAwayTeam());
            api.put("homePercent", match.getHomePercent());
            api.put("homeTeam", match.getHomeTeam());
            api.put("isAwayTorOy", match.isAwayTorOy());
            api.put("league", match.getLeague());
            api.put("neutral", match.isNeutral());
            api.put("odd", match.getOdd());
            api.put("schedule", match.getSchedule());

            response.add(api);

        }

        return ResponseEntity.ok(response);
    }

    @GetMapping("/result")
    public ResponseEntity<List<Map<String, Object>>> viewResultPage() {

        Map<String, Object> api = null;
        List<Map<String, Object>> response = new ArrayList<>();

        List<Result> results = scrapResultService.scrapResult(url);

        for (Result result : results) {
            api = new HashMap<>();
            System.out.println(result);
            api.put("awayTeam", result.getAwayTeam());
            api.put("homeTeam", result.getHomeTeam());
            
            api.put("result", result.getResult());
            
            response.add(api);

        }

        return ResponseEntity.ok(response);
    }
    @GetMapping("/result7m")
    public ResponseEntity<List<Map<String, Object>>> viewResult7MPage() {

        Map<String, Object> api = null;
        List<Map<String, Object>> response = new ArrayList<>();

        List<Result7M> results7m = scrapResult7MService.scrapResult7M(url1);

        for (Result7M result : results7m) {
            api = new HashMap<>();
            System.out.println(result);
            api.put("awayTeam", result.getAWayTeam());
            api.put("homeTeam", result.getHomeTeam());
            
            api.put("result", result.getResult());
            
            response.add(api);

        }

        return ResponseEntity.ok(response);
    }


}
