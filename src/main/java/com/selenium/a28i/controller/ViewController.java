package com.selenium.a28i.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
    

    @GetMapping("/")
    public String viewIndex() {
        return "index";
    }


}
